﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathProg
{
    public class DiffItem
    {
        public double X { get; set; }
        public double Function { get; set; }
        public double Lagrange { get; set; }
        public double Difference { get { return Math.Abs(Function - Lagrange); } }
    }
}
