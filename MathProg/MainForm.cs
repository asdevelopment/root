﻿using MathLogic;
using MathLogic.Variants;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MathProg
{
    public partial class MainForm : Form
    {
        #region Colors
        Color Normal = Color.FromArgb(85, 163, 252);
        Color Error = Color.FromArgb(252, 180, 85);
        #endregion

        List<DiffItem> Results = new List<DiffItem>();
        Series sourceFunctionSeries = new Series("Исходная функция");
        Series polynomeFunctionSeries = new Series("Функция полинома");
        AVariant variant;


        double StartXPosition { get { return Convert.ToDouble(StartXnud.Value); } }
        double EndXPosition { get { return Convert.ToDouble(EndXnud.Value); } }
        double? _step;
        double Step
        {
            get
            {
                if(!_step.HasValue)
                    _step = (EndXPosition - StartXPosition) / 100;
                return _step.Value;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            #region Initialization min/max value for updown controls
            //TODO проверить, какое число максимально лезет в decimal
            decimal incremint = Convert.ToDecimal(0.0005);
            StartXnud.Minimum = decimal.MinValue;
            EndXnud.Minimum = decimal.MinValue;
            StartXnud.Maximum = decimal.MaxValue;
            EndXnud.Maximum = decimal.MaxValue;
            StartXnud.Increment = incremint;
            EndXnud.Increment = incremint;
            #endregion
            #region Initialize graths series
            FunctionGrathsChart.Series.Clear();
            FunctionGrathsChart.ChartAreas.Add(new ChartArea("Math functions"));
            sourceFunctionSeries.ChartType = SeriesChartType.Line;
            polynomeFunctionSeries.ChartType = SeriesChartType.Line;
            sourceFunctionSeries.ChartArea = "Math functions";
            polynomeFunctionSeries.ChartArea = "Math functions";
            FunctionGrathsChart.Series.Add(sourceFunctionSeries);
            FunctionGrathsChart.Series.Add(polynomeFunctionSeries);
            polynomeFunctionSeries.Color = Color.Red;
            sourceFunctionSeries.Color = Color.DarkBlue;
            polynomeFunctionSeries.BorderWidth = 3;
            sourceFunctionSeries.BorderWidth = 3;
            #endregion
            #region Initialization combobox values
            List<string> values = new List<string>();
            values.Add("Не выбрано");
            values.Add("4");
            values.Add("6");
            values.Add("9");
            values.Add("10");
            values.Add("15");
            values.Add("17");
            values.Add("18");
            values.Add("21");
            //TODO add all variants
            VariantComboBox.DataSource = values;
            #endregion
            StatusLabel.Text = "Выберите вариант";
            MainStatusStrip.BackColor = Normal;
        }

        private void VariantComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            sourceFunctionSeries.Points.Clear();
            polynomeFunctionSeries.Points.Clear();
            CompareTabPage.Parent = null;
            PolynomeTabPage.Parent = null;
            DrawSourceFunvtionGrathButton.Enabled = false;
            try
            {
                int variantNum = Convert.ToInt16(VariantComboBox.SelectedValue);
                SourceDataPanel.Enabled = true;
                VariantLabel.Visible = false;
                switch (variantNum)
                {
                    case (4):
                        variant = new Variant4();
                        break;
                    case (6):
                        variant = new Variant6();
                        break;
                    case (9):
                        variant = new Variant9();
                        break;
                    case (10):
                        variant = new Variant10();
                        break;
                    case (15):
                        variant = new Variant15();
                        break;
                    case (17):
                        variant = new Variant17();
                        break;
                    case (18):
                        variant = new Variant18();
                        break;
                    case (21):
                        variant = new Variant21();
                        break;
                }
                FunctionLabel.Text = variant.Formule;
                FunctionLabel.Visible = true;
            }
            catch
            {
                sourceFunctionSeries.Points.Clear();
                polynomeFunctionSeries.Points.Clear();
                Results.Clear();
                _step = null;
                FunctionLabel.Visible = false;
                SourceDataPanel.Enabled = false;
                VariantLabel.Visible = true;
            }
        }

        private void DrawSourceFunvtionGrathButton_Click(object sender, EventArgs e)
        {
            DrawSourceFunctionGrath();
        }

        private void StartXLabel_Click(object sender, EventArgs e)
        {
            StartXnud.Focus();
        }

        private void EndXLabel_Click(object sender, EventArgs e)
        {
            EndXnud.Focus();
        }

        private void SourceDataPanel_Resize(object sender, EventArgs e)
        {
            int btnWidth = (int)(SourceDataPanel.Width - ConfirmSettingsButton.Margin.Horizontal
                - DropSettingsButton.Margin.Horizontal - SourceDataPanel.Padding.Horizontal) / 2;
            ConfirmSettingsButton.Location = new Point(DropSettingsButton.Location.X + DropSettingsButton.Margin.Horizontal + btnWidth
                , ConfirmSettingsButton.Location.Y);
            ConfirmSettingsButton.Width = btnWidth;
            DropSettingsButton.Width = btnWidth;
        }

        private void ConfirmSettingsButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (StartXnud.Value >= EndXnud.Value)
                    throw new ArgumentException("Не правильно задан интервал! \n" +
                        "Значение начала отрезка должно быть строго меньше значения конца отрезка");
                variant.NodePoints.Clear();
                double startX = Convert.ToDouble(StartXnud.Value),
                    endX = Convert.ToDouble(EndXnud.Value),
                    spacer = (endX - startX) / 100;
                NewNodePointNUD.Maximum = EndXnud.Value;
                NewNodePointNUD.Minimum = StartXnud.Value;
                FunctionGrathsChart.ChartAreas[0].AxisX.Maximum = endX + spacer;
                FunctionGrathsChart.ChartAreas[0].AxisX.Minimum = startX - spacer;
                variant.SetCalculatingInterval(startX, endX);
                PolynomeFuncionTextBox.Text = variant.PolinomFormule;
                CompareTabPage.Parent = DataTabControl;
                PolynomeTabPage.Parent = DataTabControl;
                PolynomeNodePointsGrid.DataSource = variant.NodePoints;
                List<FloatPoint> source = variant.CalculateFunctionValues(Step);
                List<FloatPoint> lagrange = variant.CalculatePolynomeValues(Step);
                for(int i = 0; i < source.Count; i++)
                    Results.Add(new DiffItem() { X = source[i].X, Function = source[i].Y, Lagrange = lagrange[i].Y});
                diffItemBindingSource.DataSource = Results;
                CheckGrathDIsplayInterval();
                DrawSourceFunvtionGrathButton.Enabled = true;
                Difflabel.Text = variant.CalculateDiff().ToString();
                var key = MessageBox.Show("Построить график исхоной функции?", "Построение графика", MessageBoxButtons.YesNo);
                if (key == DialogResult.Yes)
                    DrawSourceFunctionGrath();
                SetNormal();
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        private void DropSettingsButton_Click(object sender, EventArgs e)
        {
            variant = null;
            VariantComboBox.SelectedItem = VariantComboBox.Items[0];
            StartXnud.Value = Convert.ToDecimal(0);
            EndXnud.Value = Convert.ToDecimal(1);
        }

        private void ShowError(Exception e)
        {
            MessageBox.Show(e.Message, "Ошибка!");
            StatusLabel.Text = e.Message;
            MainStatusStrip.BackColor = Error;
        }

        private void DrawPolynomeFunctionGrath()
        {
            polynomeFunctionSeries.Points.Clear();
            foreach(var item in Results)
                polynomeFunctionSeries.Points.AddXY(item.X, item.Lagrange);
        }

        private void DrawSourceFunctionGrath()
        {
            sourceFunctionSeries.Points.Clear();
            //TODO разработать алгоритм подсчета оптимального значения шага для построения фунцкии
            //TODO алгоритм: циклом проходим по графику, находим высоту ступенек и делим их пополам. Соединяем линией полученные точки. Прогоняем этот цикл, пока не молучим гладкий график 
            var point = Results.First();
            double ymin  = point.Function, ymax = ymin;
            foreach (var item in Results)
                sourceFunctionSeries.Points.AddXY(item.X, item.Function);
        }

        private void AboutStatusStrpLabel_Click(object sender, EventArgs e)
        {
            (new AboutForm()).ShowDialog();
        }

        private void DrawPolynomeFunctionGrathButton_Click(object sender, EventArgs e)
        {
            DrawPolynomeFunctionGrath();
        }

        private void AddNodePointButton_Click(object sender, EventArgs e)
        {
            try
            {
                variant.AddNewNodePoint(Convert.ToDouble(NewNodePointNUD.Value));
                PolynomeNodePointsGrid.DataSource = variant.NodePoints;
                PolynomeFuncionTextBox.Text = variant.PolinomFormule;
                List<FloatPoint> lagrange = variant.CalculatePolynomeValues(Step);
                for (int i = 0; i < lagrange.Count; i++)
                    Results[i].Lagrange = lagrange[i].Y;
                diffItemBindingSource.DataSource = Results;
                CheckGrathDIsplayInterval();
                DialogResult key = MessageBox.Show("Узловые точки изменены. Перестроить график полинома?", "Построение графика", MessageBoxButtons.YesNo);
                if (key == DialogResult.Yes)
                {
                    DrawPolynomeFunctionGrath();
                    SetNormal();
                }
                else
                {
                    StatusLabel.Text = "Узловые точки изменены, график полинома не актуален.";
                    MainStatusStrip.BackColor = Error;
                }
                Difflabel.Text = variant.CalculateDiff().ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }


        private void PlynomeToolsPanel_Resize(object sender, EventArgs e)
        {
            PolynomeFuncionTextBox.Width = PolynomeToolsPanel.Width - PolynomeFuncionTextBox.Margin.Horizontal;
            PolynomeFuncionTextBox.MaxLength = (int)(PolynomeFuncionTextBox.Width / PolynomeFuncionTextBox.Font.Size);
        }

        private void PolynomeLabel_Click(object sender, EventArgs e)
        {
            if (PolynomeFuncionTextBox.Visible)
            {
                PolynomeFuncionTextBox.Visible = false;
                PolynomeToolsPanel.Height -= (PolynomeFuncionTextBox.Height + PolynomeFuncionTextBox.Margin.Vertical); 
            }
            else
            {
                PolynomeFuncionTextBox.Visible = true;
                PolynomeToolsPanel.Height += (PolynomeFuncionTextBox.Height + PolynomeFuncionTextBox.Margin.Vertical);
            }
        }

        private void CheckGrathDIsplayInterval()
        {
            double sourceMax = Results.Select(x => x.Function).Max(),
                sourceMin = Results.Select(x => x.Function).Min(),
                lagrangeMax = Results.Select(x => x.Lagrange).Max(),
                lagrangeMin = Results.Select(x => x.Lagrange).Min(),
                max = sourceMax > lagrangeMax ? sourceMax : lagrangeMax,
                min = sourceMin < lagrangeMin ? sourceMin : lagrangeMin,
                spacer = (max - min) / 100;
            FunctionGrathsChart.ChartAreas[0].AxisY.Maximum = max + spacer;
            FunctionGrathsChart.ChartAreas[0].AxisY.Minimum = min - spacer;
        }

        private void CompareTabPage_Resize(object sender, EventArgs e)
        {
            DiffDataGridView.Height = CompareTabPage.Height - (DiffGridLabel.Location.Y + DiffGridLabel.Height + DiffGridLabel.Margin.Horizontal);
        }

        private void MainSplitContainer_Resize(object sender, EventArgs e)
        {
            MainSplitContainer.Panel2MinSize = Width / 2;
        }

        private void SetNormal()
        {
            MainStatusStrip.BackColor = Normal;
            StatusLabel.Text = "Готово";
        }
    }
}
