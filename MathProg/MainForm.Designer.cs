﻿namespace MathProg
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.FunctionGrathsChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.DataTabControl = new System.Windows.Forms.TabControl();
            this.SourceTabPage = new System.Windows.Forms.TabPage();
            this.SourceDataPanel = new System.Windows.Forms.Panel();
            this.FunctionLabel = new System.Windows.Forms.Label();
            this.DropSettingsButton = new System.Windows.Forms.Button();
            this.ConfirmSettingsButton = new System.Windows.Forms.Button();
            this.DrawSourceFunvtionGrathButton = new System.Windows.Forms.Button();
            this.FunctionViewLabel = new System.Windows.Forms.Label();
            this.StartXLabel = new System.Windows.Forms.Label();
            this.StartXnud = new System.Windows.Forms.NumericUpDown();
            this.EndXLabel = new System.Windows.Forms.Label();
            this.EndXnud = new System.Windows.Forms.NumericUpDown();
            this.VariantComboBox = new System.Windows.Forms.ComboBox();
            this.VariantLabel = new System.Windows.Forms.Label();
            this.CompareTabPage = new System.Windows.Forms.TabPage();
            this.DiffGridLabel = new System.Windows.Forms.Label();
            this.DiffDataGridView = new System.Windows.Forms.DataGridView();
            this.Difflabel = new System.Windows.Forms.Label();
            this.DiffValueLabel = new System.Windows.Forms.Label();
            this.PolynomeTabPage = new System.Windows.Forms.TabPage();
            this.PolynomeNodePointsGrid = new System.Windows.Forms.DataGridView();
            this.xDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.floatPointBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PolynomeToolsPanel = new System.Windows.Forms.Panel();
            this.PolynomeFuncionTextBox = new System.Windows.Forms.TextBox();
            this.PolynomeLabel = new System.Windows.Forms.Label();
            this.DrawPolynomeFunctionGrathButton = new System.Windows.Forms.Button();
            this.AddNodePointButton = new System.Windows.Forms.Button();
            this.AddNodePointLabel = new System.Windows.Forms.Label();
            this.NewNodePointNUD = new System.Windows.Forms.NumericUpDown();
            this.MainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.MainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.AboutStatusStrpLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.xDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.functionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lagrangeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.differenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diffItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.FunctionGrathsChart)).BeginInit();
            this.DataTabControl.SuspendLayout();
            this.SourceTabPage.SuspendLayout();
            this.SourceDataPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartXnud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndXnud)).BeginInit();
            this.CompareTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DiffDataGridView)).BeginInit();
            this.PolynomeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PolynomeNodePointsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.floatPointBindingSource)).BeginInit();
            this.PolynomeToolsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NewNodePointNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitContainer)).BeginInit();
            this.MainSplitContainer.Panel1.SuspendLayout();
            this.MainSplitContainer.Panel2.SuspendLayout();
            this.MainSplitContainer.SuspendLayout();
            this.MainStatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.diffItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // FunctionGrathsChart
            // 
            this.FunctionGrathsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.FunctionGrathsChart.Legends.Add(legend1);
            this.FunctionGrathsChart.Location = new System.Drawing.Point(0, 0);
            this.FunctionGrathsChart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.FunctionGrathsChart.Name = "FunctionGrathsChart";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.FunctionGrathsChart.Series.Add(series1);
            this.FunctionGrathsChart.Size = new System.Drawing.Size(704, 597);
            this.FunctionGrathsChart.TabIndex = 0;
            this.FunctionGrathsChart.Text = "chart1";
            // 
            // DataTabControl
            // 
            this.DataTabControl.Controls.Add(this.SourceTabPage);
            this.DataTabControl.Controls.Add(this.CompareTabPage);
            this.DataTabControl.Controls.Add(this.PolynomeTabPage);
            this.DataTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataTabControl.Location = new System.Drawing.Point(0, 0);
            this.DataTabControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DataTabControl.Name = "DataTabControl";
            this.DataTabControl.SelectedIndex = 0;
            this.DataTabControl.Size = new System.Drawing.Size(351, 597);
            this.DataTabControl.TabIndex = 2;
            // 
            // SourceTabPage
            // 
            this.SourceTabPage.Controls.Add(this.SourceDataPanel);
            this.SourceTabPage.Controls.Add(this.VariantComboBox);
            this.SourceTabPage.Controls.Add(this.VariantLabel);
            this.SourceTabPage.Location = new System.Drawing.Point(4, 29);
            this.SourceTabPage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SourceTabPage.Name = "SourceTabPage";
            this.SourceTabPage.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SourceTabPage.Size = new System.Drawing.Size(343, 564);
            this.SourceTabPage.TabIndex = 0;
            this.SourceTabPage.Text = "Исх. данные";
            this.SourceTabPage.UseVisualStyleBackColor = true;
            // 
            // SourceDataPanel
            // 
            this.SourceDataPanel.Controls.Add(this.FunctionLabel);
            this.SourceDataPanel.Controls.Add(this.DropSettingsButton);
            this.SourceDataPanel.Controls.Add(this.ConfirmSettingsButton);
            this.SourceDataPanel.Controls.Add(this.DrawSourceFunvtionGrathButton);
            this.SourceDataPanel.Controls.Add(this.FunctionViewLabel);
            this.SourceDataPanel.Controls.Add(this.StartXLabel);
            this.SourceDataPanel.Controls.Add(this.StartXnud);
            this.SourceDataPanel.Controls.Add(this.EndXLabel);
            this.SourceDataPanel.Controls.Add(this.EndXnud);
            this.SourceDataPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.SourceDataPanel.Location = new System.Drawing.Point(4, 53);
            this.SourceDataPanel.Name = "SourceDataPanel";
            this.SourceDataPanel.Size = new System.Drawing.Size(335, 257);
            this.SourceDataPanel.TabIndex = 9;
            this.SourceDataPanel.Resize += new System.EventHandler(this.SourceDataPanel_Resize);
            // 
            // FunctionLabel
            // 
            this.FunctionLabel.AutoSize = true;
            this.FunctionLabel.Location = new System.Drawing.Point(18, 40);
            this.FunctionLabel.Name = "FunctionLabel";
            this.FunctionLabel.Size = new System.Drawing.Size(110, 20);
            this.FunctionLabel.TabIndex = 11;
            this.FunctionLabel.Text = "FunctionLabel";
            this.FunctionLabel.Visible = false;
            // 
            // DropSettingsButton
            // 
            this.DropSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DropSettingsButton.Location = new System.Drawing.Point(8, 163);
            this.DropSettingsButton.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.DropSettingsButton.Name = "DropSettingsButton";
            this.DropSettingsButton.Size = new System.Drawing.Size(125, 41);
            this.DropSettingsButton.TabIndex = 10;
            this.DropSettingsButton.Text = "Сбросить";
            this.DropSettingsButton.UseVisualStyleBackColor = true;
            this.DropSettingsButton.Click += new System.EventHandler(this.DropSettingsButton_Click);
            // 
            // ConfirmSettingsButton
            // 
            this.ConfirmSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ConfirmSettingsButton.Location = new System.Drawing.Point(201, 163);
            this.ConfirmSettingsButton.Margin = new System.Windows.Forms.Padding(3, 3, 7, 3);
            this.ConfirmSettingsButton.Name = "ConfirmSettingsButton";
            this.ConfirmSettingsButton.Size = new System.Drawing.Size(127, 41);
            this.ConfirmSettingsButton.TabIndex = 9;
            this.ConfirmSettingsButton.Text = "Подтвердить";
            this.ConfirmSettingsButton.UseVisualStyleBackColor = true;
            this.ConfirmSettingsButton.Click += new System.EventHandler(this.ConfirmSettingsButton_Click);
            // 
            // DrawSourceFunvtionGrathButton
            // 
            this.DrawSourceFunvtionGrathButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DrawSourceFunvtionGrathButton.Location = new System.Drawing.Point(0, 215);
            this.DrawSourceFunvtionGrathButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DrawSourceFunvtionGrathButton.Name = "DrawSourceFunvtionGrathButton";
            this.DrawSourceFunvtionGrathButton.Size = new System.Drawing.Size(335, 42);
            this.DrawSourceFunvtionGrathButton.TabIndex = 5;
            this.DrawSourceFunvtionGrathButton.Text = "Построить график";
            this.DrawSourceFunvtionGrathButton.UseVisualStyleBackColor = true;
            this.DrawSourceFunvtionGrathButton.Click += new System.EventHandler(this.DrawSourceFunvtionGrathButton_Click);
            // 
            // FunctionViewLabel
            // 
            this.FunctionViewLabel.AutoSize = true;
            this.FunctionViewLabel.Location = new System.Drawing.Point(14, 16);
            this.FunctionViewLabel.Name = "FunctionViewLabel";
            this.FunctionViewLabel.Size = new System.Drawing.Size(168, 20);
            this.FunctionViewLabel.TabIndex = 8;
            this.FunctionViewLabel.Text = "Выбранная функция:";
            // 
            // StartXLabel
            // 
            this.StartXLabel.AutoSize = true;
            this.StartXLabel.Location = new System.Drawing.Point(14, 81);
            this.StartXLabel.Name = "StartXLabel";
            this.StartXLabel.Size = new System.Drawing.Size(154, 20);
            this.StartXLabel.TabIndex = 6;
            this.StartXLabel.Text = "Введите начало X:";
            this.StartXLabel.Click += new System.EventHandler(this.StartXLabel_Click);
            // 
            // StartXnud
            // 
            this.StartXnud.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StartXnud.DecimalPlaces = 4;
            this.StartXnud.Location = new System.Drawing.Point(226, 79);
            this.StartXnud.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.StartXnud.Name = "StartXnud";
            this.StartXnud.Size = new System.Drawing.Size(105, 26);
            this.StartXnud.TabIndex = 1;
            // 
            // EndXLabel
            // 
            this.EndXLabel.AutoSize = true;
            this.EndXLabel.Location = new System.Drawing.Point(14, 117);
            this.EndXLabel.Name = "EndXLabel";
            this.EndXLabel.Size = new System.Drawing.Size(143, 20);
            this.EndXLabel.TabIndex = 7;
            this.EndXLabel.Text = "Введите конец X:";
            this.EndXLabel.Click += new System.EventHandler(this.EndXLabel_Click);
            // 
            // EndXnud
            // 
            this.EndXnud.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EndXnud.DecimalPlaces = 4;
            this.EndXnud.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.EndXnud.Location = new System.Drawing.Point(226, 115);
            this.EndXnud.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.EndXnud.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EndXnud.Name = "EndXnud";
            this.EndXnud.Size = new System.Drawing.Size(105, 26);
            this.EndXnud.TabIndex = 2;
            // 
            // VariantComboBox
            // 
            this.VariantComboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.VariantComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VariantComboBox.FormattingEnabled = true;
            this.VariantComboBox.Location = new System.Drawing.Point(4, 25);
            this.VariantComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.VariantComboBox.Name = "VariantComboBox";
            this.VariantComboBox.Size = new System.Drawing.Size(335, 28);
            this.VariantComboBox.TabIndex = 0;
            this.VariantComboBox.SelectedValueChanged += new System.EventHandler(this.VariantComboBox_SelectedValueChanged);
            // 
            // VariantLabel
            // 
            this.VariantLabel.AutoSize = true;
            this.VariantLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.VariantLabel.Location = new System.Drawing.Point(4, 5);
            this.VariantLabel.Name = "VariantLabel";
            this.VariantLabel.Size = new System.Drawing.Size(143, 20);
            this.VariantLabel.TabIndex = 8;
            this.VariantLabel.Text = "Выбрите вариант";
            // 
            // CompareTabPage
            // 
            this.CompareTabPage.Controls.Add(this.DiffGridLabel);
            this.CompareTabPage.Controls.Add(this.DiffDataGridView);
            this.CompareTabPage.Controls.Add(this.Difflabel);
            this.CompareTabPage.Controls.Add(this.DiffValueLabel);
            this.CompareTabPage.Location = new System.Drawing.Point(4, 29);
            this.CompareTabPage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CompareTabPage.Name = "CompareTabPage";
            this.CompareTabPage.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CompareTabPage.Size = new System.Drawing.Size(343, 564);
            this.CompareTabPage.TabIndex = 1;
            this.CompareTabPage.Text = "Сравнение";
            this.CompareTabPage.UseVisualStyleBackColor = true;
            this.CompareTabPage.Resize += new System.EventHandler(this.CompareTabPage_Resize);
            // 
            // DiffGridLabel
            // 
            this.DiffGridLabel.AutoSize = true;
            this.DiffGridLabel.Location = new System.Drawing.Point(8, 76);
            this.DiffGridLabel.Name = "DiffGridLabel";
            this.DiffGridLabel.Size = new System.Drawing.Size(232, 20);
            this.DiffGridLabel.TabIndex = 12;
            this.DiffGridLabel.Text = "Таблица сравнений значений";
            // 
            // DiffDataGridView
            // 
            this.DiffDataGridView.AllowUserToAddRows = false;
            this.DiffDataGridView.AllowUserToDeleteRows = false;
            this.DiffDataGridView.AllowUserToResizeRows = false;
            this.DiffDataGridView.AutoGenerateColumns = false;
            this.DiffDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DiffDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.xDataGridViewTextBoxColumn1,
            this.functionDataGridViewTextBoxColumn,
            this.lagrangeDataGridViewTextBoxColumn,
            this.differenceDataGridViewTextBoxColumn});
            this.DiffDataGridView.DataSource = this.diffItemBindingSource;
            this.DiffDataGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DiffDataGridView.Location = new System.Drawing.Point(4, 110);
            this.DiffDataGridView.Name = "DiffDataGridView";
            this.DiffDataGridView.ReadOnly = true;
            this.DiffDataGridView.RowTemplate.Height = 28;
            this.DiffDataGridView.Size = new System.Drawing.Size(335, 449);
            this.DiffDataGridView.TabIndex = 11;
            // 
            // Difflabel
            // 
            this.Difflabel.AutoSize = true;
            this.Difflabel.Location = new System.Drawing.Point(8, 47);
            this.Difflabel.Name = "Difflabel";
            this.Difflabel.Size = new System.Drawing.Size(67, 20);
            this.Difflabel.TabIndex = 10;
            this.Difflabel.Text = "Difflabel";
            // 
            // DiffValueLabel
            // 
            this.DiffValueLabel.AutoSize = true;
            this.DiffValueLabel.Location = new System.Drawing.Point(8, 16);
            this.DiffValueLabel.Name = "DiffValueLabel";
            this.DiffValueLabel.Size = new System.Drawing.Size(175, 20);
            this.DiffValueLabel.TabIndex = 9;
            this.DiffValueLabel.Text = "Разность сумматоров";
            // 
            // PolynomeTabPage
            // 
            this.PolynomeTabPage.Controls.Add(this.PolynomeNodePointsGrid);
            this.PolynomeTabPage.Controls.Add(this.PolynomeToolsPanel);
            this.PolynomeTabPage.Location = new System.Drawing.Point(4, 29);
            this.PolynomeTabPage.Name = "PolynomeTabPage";
            this.PolynomeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.PolynomeTabPage.Size = new System.Drawing.Size(343, 564);
            this.PolynomeTabPage.TabIndex = 2;
            this.PolynomeTabPage.Text = "Полином";
            this.PolynomeTabPage.UseVisualStyleBackColor = true;
            // 
            // PolynomeNodePointsGrid
            // 
            this.PolynomeNodePointsGrid.AllowUserToAddRows = false;
            this.PolynomeNodePointsGrid.AutoGenerateColumns = false;
            this.PolynomeNodePointsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PolynomeNodePointsGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.PolynomeNodePointsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PolynomeNodePointsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.xDataGridViewTextBoxColumn,
            this.yDataGridViewTextBoxColumn});
            this.PolynomeNodePointsGrid.DataSource = this.floatPointBindingSource;
            this.PolynomeNodePointsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PolynomeNodePointsGrid.Location = new System.Drawing.Point(3, 140);
            this.PolynomeNodePointsGrid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PolynomeNodePointsGrid.Name = "PolynomeNodePointsGrid";
            this.PolynomeNodePointsGrid.ReadOnly = true;
            this.PolynomeNodePointsGrid.Size = new System.Drawing.Size(337, 421);
            this.PolynomeNodePointsGrid.TabIndex = 14;
            // 
            // xDataGridViewTextBoxColumn
            // 
            this.xDataGridViewTextBoxColumn.DataPropertyName = "X";
            this.xDataGridViewTextBoxColumn.HeaderText = "X";
            this.xDataGridViewTextBoxColumn.Name = "xDataGridViewTextBoxColumn";
            this.xDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // yDataGridViewTextBoxColumn
            // 
            this.yDataGridViewTextBoxColumn.DataPropertyName = "Y";
            this.yDataGridViewTextBoxColumn.HeaderText = "Y";
            this.yDataGridViewTextBoxColumn.Name = "yDataGridViewTextBoxColumn";
            this.yDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // floatPointBindingSource
            // 
            this.floatPointBindingSource.DataSource = typeof(MathLogic.FloatPoint);
            // 
            // PolynomeToolsPanel
            // 
            this.PolynomeToolsPanel.Controls.Add(this.PolynomeFuncionTextBox);
            this.PolynomeToolsPanel.Controls.Add(this.PolynomeLabel);
            this.PolynomeToolsPanel.Controls.Add(this.DrawPolynomeFunctionGrathButton);
            this.PolynomeToolsPanel.Controls.Add(this.AddNodePointButton);
            this.PolynomeToolsPanel.Controls.Add(this.AddNodePointLabel);
            this.PolynomeToolsPanel.Controls.Add(this.NewNodePointNUD);
            this.PolynomeToolsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.PolynomeToolsPanel.Location = new System.Drawing.Point(3, 3);
            this.PolynomeToolsPanel.Name = "PolynomeToolsPanel";
            this.PolynomeToolsPanel.Size = new System.Drawing.Size(337, 137);
            this.PolynomeToolsPanel.TabIndex = 13;
            this.PolynomeToolsPanel.Resize += new System.EventHandler(this.PlynomeToolsPanel_Resize);
            // 
            // PolynomeFuncionTextBox
            // 
            this.PolynomeFuncionTextBox.BackColor = System.Drawing.SystemColors.Menu;
            this.PolynomeFuncionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PolynomeFuncionTextBox.Location = new System.Drawing.Point(7, 23);
            this.PolynomeFuncionTextBox.MaxLength = 20;
            this.PolynomeFuncionTextBox.Multiline = true;
            this.PolynomeFuncionTextBox.Name = "PolynomeFuncionTextBox";
            this.PolynomeFuncionTextBox.ReadOnly = true;
            this.PolynomeFuncionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.PolynomeFuncionTextBox.Size = new System.Drawing.Size(309, 94);
            this.PolynomeFuncionTextBox.TabIndex = 13;
            this.PolynomeFuncionTextBox.Visible = false;
            // 
            // PolynomeLabel
            // 
            this.PolynomeLabel.AutoSize = true;
            this.PolynomeLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PolynomeLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.PolynomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PolynomeLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.PolynomeLabel.Location = new System.Drawing.Point(0, 0);
            this.PolynomeLabel.Name = "PolynomeLabel";
            this.PolynomeLabel.Size = new System.Drawing.Size(193, 20);
            this.PolynomeLabel.TabIndex = 0;
            this.PolynomeLabel.Text = "Вид функции полинома:";
            this.PolynomeLabel.Click += new System.EventHandler(this.PolynomeLabel_Click);
            // 
            // DrawPolynomeFunctionGrathButton
            // 
            this.DrawPolynomeFunctionGrathButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DrawPolynomeFunctionGrathButton.Location = new System.Drawing.Point(0, 95);
            this.DrawPolynomeFunctionGrathButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DrawPolynomeFunctionGrathButton.Name = "DrawPolynomeFunctionGrathButton";
            this.DrawPolynomeFunctionGrathButton.Size = new System.Drawing.Size(337, 42);
            this.DrawPolynomeFunctionGrathButton.TabIndex = 9;
            this.DrawPolynomeFunctionGrathButton.Text = "Построить полином";
            this.DrawPolynomeFunctionGrathButton.UseVisualStyleBackColor = true;
            this.DrawPolynomeFunctionGrathButton.Click += new System.EventHandler(this.DrawPolynomeFunctionGrathButton_Click);
            // 
            // AddNodePointButton
            // 
            this.AddNodePointButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AddNodePointButton.Location = new System.Drawing.Point(197, 54);
            this.AddNodePointButton.Name = "AddNodePointButton";
            this.AddNodePointButton.Size = new System.Drawing.Size(119, 33);
            this.AddNodePointButton.TabIndex = 10;
            this.AddNodePointButton.Text = "Добавить";
            this.AddNodePointButton.UseVisualStyleBackColor = true;
            this.AddNodePointButton.Click += new System.EventHandler(this.AddNodePointButton_Click);
            // 
            // AddNodePointLabel
            // 
            this.AddNodePointLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddNodePointLabel.AutoSize = true;
            this.AddNodePointLabel.Location = new System.Drawing.Point(0, 31);
            this.AddNodePointLabel.Name = "AddNodePointLabel";
            this.AddNodePointLabel.Size = new System.Drawing.Size(200, 20);
            this.AddNodePointLabel.TabIndex = 11;
            this.AddNodePointLabel.Text = "Добавить узловую точку:";
            // 
            // NewNodePointNUD
            // 
            this.NewNodePointNUD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.NewNodePointNUD.Location = new System.Drawing.Point(11, 61);
            this.NewNodePointNUD.Name = "NewNodePointNUD";
            this.NewNodePointNUD.Size = new System.Drawing.Size(120, 26);
            this.NewNodePointNUD.TabIndex = 12;
            // 
            // MainSplitContainer
            // 
            this.MainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.MainSplitContainer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MainSplitContainer.Name = "MainSplitContainer";
            // 
            // MainSplitContainer.Panel1
            // 
            this.MainSplitContainer.Panel1.Controls.Add(this.DataTabControl);
            this.MainSplitContainer.Panel1MinSize = 250;
            // 
            // MainSplitContainer.Panel2
            // 
            this.MainSplitContainer.Panel2.Controls.Add(this.FunctionGrathsChart);
            this.MainSplitContainer.Size = new System.Drawing.Size(1061, 597);
            this.MainSplitContainer.SplitterDistance = 351;
            this.MainSplitContainer.SplitterWidth = 6;
            this.MainSplitContainer.TabIndex = 3;
            this.MainSplitContainer.TabStop = false;
            this.MainSplitContainer.Resize += new System.EventHandler(this.MainSplitContainer_Resize);
            // 
            // MainStatusStrip
            // 
            this.MainStatusStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.MainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutStatusStrpLabel,
            this.StatusLabel});
            this.MainStatusStrip.Location = new System.Drawing.Point(0, 597);
            this.MainStatusStrip.Name = "MainStatusStrip";
            this.MainStatusStrip.Size = new System.Drawing.Size(1061, 30);
            this.MainStatusStrip.TabIndex = 4;
            this.MainStatusStrip.Text = "MainStatusStrip";
            // 
            // AboutStatusStrpLabel
            // 
            this.AboutStatusStrpLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Underline);
            this.AboutStatusStrpLabel.Name = "AboutStatusStrpLabel";
            this.AboutStatusStrpLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.AboutStatusStrpLabel.Size = new System.Drawing.Size(112, 25);
            this.AboutStatusStrpLabel.Text = "О програме";
            this.AboutStatusStrpLabel.Click += new System.EventHandler(this.AboutStatusStrpLabel_Click);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(63, 25);
            this.StatusLabel.Text = "Статус";
            // 
            // xDataGridViewTextBoxColumn1
            // 
            this.xDataGridViewTextBoxColumn1.DataPropertyName = "X";
            this.xDataGridViewTextBoxColumn1.HeaderText = "X";
            this.xDataGridViewTextBoxColumn1.Name = "xDataGridViewTextBoxColumn1";
            this.xDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // functionDataGridViewTextBoxColumn
            // 
            this.functionDataGridViewTextBoxColumn.DataPropertyName = "Function";
            this.functionDataGridViewTextBoxColumn.HeaderText = "Function";
            this.functionDataGridViewTextBoxColumn.Name = "functionDataGridViewTextBoxColumn";
            this.functionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lagrangeDataGridViewTextBoxColumn
            // 
            this.lagrangeDataGridViewTextBoxColumn.DataPropertyName = "Lagrange";
            this.lagrangeDataGridViewTextBoxColumn.HeaderText = "Lagrange";
            this.lagrangeDataGridViewTextBoxColumn.Name = "lagrangeDataGridViewTextBoxColumn";
            this.lagrangeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // differenceDataGridViewTextBoxColumn
            // 
            this.differenceDataGridViewTextBoxColumn.DataPropertyName = "Difference";
            this.differenceDataGridViewTextBoxColumn.HeaderText = "Difference";
            this.differenceDataGridViewTextBoxColumn.Name = "differenceDataGridViewTextBoxColumn";
            this.differenceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // diffItemBindingSource
            // 
            this.diffItemBindingSource.DataSource = typeof(MathProg.DiffItem);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 627);
            this.Controls.Add(this.MainSplitContainer);
            this.Controls.Add(this.MainStatusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(900, 640);
            this.Name = "MainForm";
            this.Text = "Построение полиномов апроксимации";
            ((System.ComponentModel.ISupportInitialize)(this.FunctionGrathsChart)).EndInit();
            this.DataTabControl.ResumeLayout(false);
            this.SourceTabPage.ResumeLayout(false);
            this.SourceTabPage.PerformLayout();
            this.SourceDataPanel.ResumeLayout(false);
            this.SourceDataPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartXnud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndXnud)).EndInit();
            this.CompareTabPage.ResumeLayout(false);
            this.CompareTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DiffDataGridView)).EndInit();
            this.PolynomeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PolynomeNodePointsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.floatPointBindingSource)).EndInit();
            this.PolynomeToolsPanel.ResumeLayout(false);
            this.PolynomeToolsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NewNodePointNUD)).EndInit();
            this.MainSplitContainer.Panel1.ResumeLayout(false);
            this.MainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitContainer)).EndInit();
            this.MainSplitContainer.ResumeLayout(false);
            this.MainStatusStrip.ResumeLayout(false);
            this.MainStatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.diffItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart FunctionGrathsChart;
        private System.Windows.Forms.TabControl DataTabControl;
        private System.Windows.Forms.TabPage SourceTabPage;
        private System.Windows.Forms.Button DrawSourceFunvtionGrathButton;
        private System.Windows.Forms.NumericUpDown EndXnud;
        private System.Windows.Forms.NumericUpDown StartXnud;
        private System.Windows.Forms.ComboBox VariantComboBox;
        private System.Windows.Forms.TabPage CompareTabPage;
        private System.Windows.Forms.SplitContainer MainSplitContainer;
        private System.Windows.Forms.Label EndXLabel;
        private System.Windows.Forms.Label StartXLabel;
        private System.Windows.Forms.Label VariantLabel;
        private System.Windows.Forms.Panel SourceDataPanel;
        private System.Windows.Forms.Label FunctionViewLabel;
        private System.Windows.Forms.Button DropSettingsButton;
        private System.Windows.Forms.Button ConfirmSettingsButton;
        private System.Windows.Forms.Label FunctionLabel;
        private System.Windows.Forms.StatusStrip MainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel AboutStatusStrpLabel;
        private System.Windows.Forms.TabPage PolynomeTabPage;
        private System.Windows.Forms.BindingSource floatPointBindingSource;
        private System.Windows.Forms.Label PolynomeLabel;
        private System.Windows.Forms.Label Difflabel;
        private System.Windows.Forms.Label DiffValueLabel;
        private System.Windows.Forms.DataGridView PolynomeNodePointsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn xDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel PolynomeToolsPanel;
        private System.Windows.Forms.Button DrawPolynomeFunctionGrathButton;
        private System.Windows.Forms.Button AddNodePointButton;
        private System.Windows.Forms.Label AddNodePointLabel;
        private System.Windows.Forms.NumericUpDown NewNodePointNUD;
        private System.Windows.Forms.TextBox PolynomeFuncionTextBox;
        private System.Windows.Forms.DataGridView DiffDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn xDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn functionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lagrangeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn differenceDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource diffItemBindingSource;
        private System.Windows.Forms.Label DiffGridLabel;
    }
}

