﻿using System;

namespace MathLogic.Variants
{
    public class Variant6 : AVariant
    {
        public override string Formule => "(7 * (x^2 + 1) ^ 0.5) / x ^ 4";

        protected override double CalculateFunctionValue(double x)
        {
            return (7 * Math.Pow((Math.Pow(x, 2) +1), 0.5)) / Math.Pow(x, 4.0) ;
        }

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            if (startX <= 0 && endX >= 0)
                return false;
            return true;
        }
    }
}
