﻿using System;

namespace MathLogic.Variants
{
    public class Variant21: AVariant
    {
        public override string Formule => "8.3 * (sin(x))/(3 + cos(x))";

        protected override double CalculateFunctionValue(double x)
        {
            return 8.3 * Math.Sin(x) / (3 + Math.Cos(x));
        }

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            return true;
        }
    }
}
