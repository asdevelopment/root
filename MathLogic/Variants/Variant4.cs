﻿using System;

namespace MathLogic.Variants
{
    public class Variant4 : AVariant
    {
        public override string Formule => "5,5 * (4 + x^2)^0.33";

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            return true;
        }

        protected override double CalculateFunctionValue(double x)
        {
            return 5.5 * Math.Pow(4 + Math.Pow(x, 2), 0.33);
        }
    }
}
