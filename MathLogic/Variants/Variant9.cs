﻿using System;

namespace MathLogic.Variants
{
    public class Variant9 : AVariant
    {
        public override string Formule => "2.4 * cos(e^(-x/3))";

        protected override double CalculateFunctionValue(double x)
        {
            return 2.4 * Math.Cos(Math.Pow(Math.E, (-x / 3)));
        }

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            return true;
        }
    }
}
