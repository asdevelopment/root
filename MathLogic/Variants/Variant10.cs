﻿using System;

namespace MathLogic.Variants
{
    public class Variant10 : AVariant
    {
        public override string Formule => "(e^((3x +7)^0.5))/(x^2 + 1)";

        protected override double CalculateFunctionValue(double x)
        {
            return (Math.Pow(Math.E, Math.Pow(3*x + 7, 0.5)))/(Math.Pow(x, 2.0) + 1);
        }

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            if (startX < -(7 / 3))
                return false;
            return true;
        }
    }   
}
