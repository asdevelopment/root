﻿using System;

namespace MathLogic.Variants
{
    public class Variant15 : AVariant
    {
        public override string Formule => "cos((4.1 * (x^3 +1)^0.5)/x^2)";

        protected override double CalculateFunctionValue(double x)
        {
            return Math.Cos(4.1 * Math.Pow((Math.Pow(x, 3.0) + 1), 0.5) / Math.Pow(x, 2.0));
        }

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            if (startX <= -1)
                return false;
            if (startX <= 0 && endX >= 0)
                return false;
            return true;
        }
    }
}
