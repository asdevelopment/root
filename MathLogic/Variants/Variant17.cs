﻿using System;

namespace MathLogic.Variants
{
    public sealed class Variant17 : AVariant
    {
        public Variant17() 
            : base()
        {
        }

        public override string Formule => "y = 3 * e^(5 + sqrt(x^2 + 3))";

        protected override double CalculateFunctionValue(double x)
        {
            return (float)(3 * Math.Exp(5 + Math.Sqrt(Math.Pow(x, 2) + 3)));
        }

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            return true;
        }
    }
}
