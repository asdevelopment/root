﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLogic.Variants
{
    public class Variant18 : AVariant
    {
        public override string Formule => "2.3 * cos((5x-3)/7)";

        protected override double CalculateFunctionValue(double x)
        {
            return 2.3 * Math.Cos((5 * x - 3) / 7);
        }

        protected override bool CheckAdmissibleValues(double startX, double endX)
        {
            return true;
        }
    }
}
