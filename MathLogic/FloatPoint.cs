﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLogic
{
    public class FloatPoint : IComparable<FloatPoint>
    {
        public double X { get; set; }
        public double Y { get; set; }

        public FloatPoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public int CompareTo(FloatPoint other)
        {
            if (other == null)
                return 1;
            else
                return this.X.CompareTo(other.X);
        }
    }
}
