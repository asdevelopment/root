﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace MathLogic
{
    public abstract class AVariant
    {
        /// <summary>
        /// Строковое значение функции
        /// </summary>
        public virtual string Formule { get; }

        /// <summary>
        /// Строковое значение функции полинома
        /// Работает - не трогай! (с) Влад
        /// </summary>
        public string PolinomFormule
        {
            get
            {
                string result = "L(x) = ";
                for (int i = 0; i < Coefficients.Count; i++)
                {
                    if (Coefficients[i] == 0)
                        continue;
                    result += "(";
                    result += $"{Math.Round(Coefficients[i], 3)} * ";
                    for (int j = 0; j < _nodePoints.Count; j++)
                    {
                        if (i != j)
                        {
                            if (j != 0)
                                result += " * ";
                            result += _nodePoints[j].X == 0 ? "x" : $"(x-{_nodePoints[j].X})";
                        }
                    }
                    result += ")";
                    if (i != Coefficients.Count - 1)
                        result += " + ";
                }
                return result;
            }
        }

        /// <summary>
        /// Список коэффицентов A
        /// </summary>
        private List<double> Coefficients;

        /// <summary>
        /// Список узловых точек
        /// </summary>
        private List<FloatPoint> _nodePoints;

        public BindingList<FloatPoint> NodePoints => new BindingList<FloatPoint>(_nodePoints);
        #region Protected Methods
        protected AVariant()
        {
            _nodePoints = new List<FloatPoint>();
        }

        /// <summary>
        /// Метод подсчета значения функции.
        /// Переопределяется в каждом варианте, для реализации отдельно взятоой функции
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        protected virtual double CalculateFunctionValue(double x)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Метод проверки ОДЗ функции
        /// </summary>
        /// <returns></returns>
        protected virtual bool CheckAdmissibleValues(double startX, double endX)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Функция подсчета значения полинома функции в точке
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private double CalculatePolinomValue(double x)
        {
            double res = 0;
            for (int i = 0; i < Coefficients.Count; i++)
            {
                double temp = Coefficients[i];
                for (int j = 0; j < _nodePoints.Count; j++)
                {
                    if (j != i)
                        temp *= x - _nodePoints[j].X;
                }
                res += temp;
            }
            return res;
        }

        /// <summary>
        /// Функция подсчета перечня коэффииентов
        /// </summary>
        private void CalculateCoefficients()
        {
            Coefficients = new List<double>(_nodePoints.Count);
            for (int i = 0; i < _nodePoints.Count; i++)
            {
                double value = CalculateFunctionValue(_nodePoints[i].X);
                double temp = 1;
                for (int j = 0; j < _nodePoints.Count; j++)
                {
                    if (i != j)
                    {
                        temp *= (_nodePoints[i].X - _nodePoints[j].X);
                    }
                }
                value /= temp;
                Coefficients.Add(value);
            }
        }
        #endregion

        #region Public Methods
        public void SetCalculatingInterval(double startX, double endX)
        {
            if (!CheckAdmissibleValues(startX, endX))
                throw new ArgumentException($"Как мимимум одно значение из входного интервала [{startX};{endX}] не удовлетворяет ОДЗ");
            _nodePoints.Add(new FloatPoint(startX, CalculateFunctionValue(startX)));
            _nodePoints.Add(new FloatPoint(endX, CalculateFunctionValue(endX)));
            CalculateCoefficients();
        }

        public void AddNewNodePoint(double x)
        {
            if (_nodePoints[0].X >= x || _nodePoints[_nodePoints.Count - 1].X <= x)
                throw new ArgumentException($"Точка {x} находится за пределами или на границе определённого ранее отрезка");
            if (_nodePoints.FirstOrDefault(node => node.X == x) != null)
                throw new ArgumentException($"Введенная точка {x} совпадает с уже введённой узловой точкой");
            _nodePoints.Add(new FloatPoint(x, CalculateFunctionValue(x)));
            _nodePoints.Sort();
            CalculateCoefficients();
        }

        public List<FloatPoint> CalculateFunctionValues(double step)
        {
            List<FloatPoint> result = new List<FloatPoint>();
            for (double val = _nodePoints[0].X; val <= _nodePoints[_nodePoints.Count - 1].X; val += step)
                result.Add(new FloatPoint(val, CalculateFunctionValue(val)));
            result.Sort();
            return result;
        }

        public List<FloatPoint> CalculatePolynomeValues(double step)
        {
            List<FloatPoint> result = new List<FloatPoint>();
            for (double val = _nodePoints[0].X; val <= _nodePoints[_nodePoints.Count - 1].X; val += step)
                result.Add(new FloatPoint(val, CalculatePolinomValue(val)));
            result.Sort();
            return result;
        }

        public double CalculateDiff()
        {
            double result = 0.0;
            //TODO добавить многопоточность в реализации метода
            //TODO подумаьб над оптимаьностью шага вычислений
            double step = (_nodePoints[_nodePoints.Count - 1].X - _nodePoints[0].X) / 200;
            for (double cur = _nodePoints[0].X; cur < _nodePoints[_nodePoints.Count - 1].X; cur += step )
            {
                result += Math.Abs(CalculateFunctionValue(cur + step / 2) * (step) - CalculatePolinomValue(cur + step / 2) * (step));
            }
            return result;
        }
        #endregion
    }
}
